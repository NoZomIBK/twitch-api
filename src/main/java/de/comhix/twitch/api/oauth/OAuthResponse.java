package de.comhix.twitch.api.oauth;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OAuthResponse {
    @SerializedName("scope")
    private List<String> scope;

    @SerializedName("access_token")
    private String accessToken;

    public OAuthResponse(String accessToken, List<String> scope) {
        this.accessToken = accessToken;
        this.scope = scope;
    }

    public List<String> getScope() {
        return scope;
    }

    public void setScope(List<String> scope) {
        this.scope = scope;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
