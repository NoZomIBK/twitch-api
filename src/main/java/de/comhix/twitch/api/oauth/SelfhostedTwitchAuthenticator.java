package de.comhix.twitch.api.oauth;

import com.google.common.base.Splitter;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author Benjamin Beeker
 */
public class SelfhostedTwitchAuthenticator {
    private static final Logger log = LoggerFactory.getLogger(SelfhostedTwitchAuthenticator.class);

    public static final int DEFAULT_PORT = 9090;
    public static final String RESPONSE_HTML = "<body onload=\"window.close()\">";
    private static final String REDIRECT_URI = "http://localhost:%d/oauth";

    private final ClientInformation clientInformation;
    private final String landingHTML;
    private final int port;

    public SelfhostedTwitchAuthenticator(ClientInformation clientInformation, String landingHTML, int port) {
        this.clientInformation = clientInformation;
        this.landingHTML = landingHTML;
        this.port = port;
    }

    public Observable<OAuthResponse> doOAuth() {
        String callbackUrl = String.format(REDIRECT_URI, port);
        TwitchAuthenticator twitchAuthenticator = new TwitchAuthenticator(clientInformation,callbackUrl);

        TwitchAuthenticator.SecondStep secondStep = twitchAuthenticator.doAuth();

        try {
            Desktop.getDesktop().browse(new URI(secondStep.getTargetUrl()));
        }
        catch (IOException | URISyntaxException e) {
            log.error(e.getMessage(), e);
            return Observable.error(e);
        }

        CompletableFuture<OAuthResponse> someFuture = new CompletableFuture<>();
        HttpServer server = HttpServer.createSimpleServer(".", port);
        server.getServerConfiguration().addHttpHandler(
                new HttpHandler() {
                    public void service(Request request, Response response) throws Exception {
                        String queryString = request.getQueryString();

                        Map<String, String> queryParams = Splitter.on("&").withKeyValueSeparator("=").split(queryString);

                        String error = queryParams.get("error");
                        if (error != null) {
                            someFuture.completeExceptionally(new IllegalStateException("error on oauth-process: " + error + " (" + queryParams.get("error_description") + ")"));
                        }
                        else {
                            String authCode = queryParams.get("code");
                            secondStep.getOAuthToken(authCode)
                                    .subscribe(someFuture::complete, someFuture::completeExceptionally);
                        }
                        response.setContentType("text/html");
                        response.setContentLength(landingHTML.length());
                        response.getWriter().write(landingHTML);
                        server.shutdown();
                    }
                });
        try {
            log.info("starting server");
            server.start();
        }
        catch (Exception e) {
            log.error("error starting server", e);
            return Observable.error(e);
        }
        return Observable.fromFuture(someFuture).observeOn(Schedulers.newThread());
    }
}
