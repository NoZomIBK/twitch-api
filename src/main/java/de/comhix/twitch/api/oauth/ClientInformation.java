package de.comhix.twitch.api.oauth;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.SerializedName;

public class ClientInformation {

    @SerializedName("client-id")
    private final String clientId;

    @SerializedName("client-secret")
    private final String clientSecret;

    @SerializedName("callback-url")
    private final String callbackUrl;

    public ClientInformation(String clientId, String clientSecret) {
        this(clientId, clientSecret, null);
    }

    public ClientInformation(String clientId, String clientSecret, String callbackUrl) {
        this.clientId = Preconditions.checkNotNull(clientId,"clientId may not be null");
        this.clientSecret = Preconditions.checkNotNull(clientSecret,"clientSecret may not be null");
        this.callbackUrl = callbackUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    @Override
    public String toString() {
        return "ClientInformation{" +
                "clientId='" + clientId + '\'' +
                ", clientSecret='" + clientSecret + '\'' +
                ", callbackUrl='" + callbackUrl + '\'' +
                '}';
    }
}
