package de.comhix.twitch.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Benjamin Beeker
 */
public class UsersResponse {
    @SerializedName("_total")
    public int total;
    public List<User> users;
}
