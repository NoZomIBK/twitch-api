package de.comhix.twitch.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * @author Benjamin Beeker
 */
public class DetailedUser extends User {
    private String email;
    @SerializedName("email_verified")
    private boolean emailVerified;
    private boolean partnered;

    private Notifications notifications;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public boolean isPartnered() {
        return partnered;
    }

    public void setPartnered(boolean partnered) {
        this.partnered = partnered;
    }

    public Notifications getNotifications() {
        return notifications;
    }

    public void setNotifications(Notifications notifications) {
        this.notifications = notifications;
    }

    public static class Notifications {
        private boolean email;
        private boolean push;

        public boolean isPush() {
            return push;
        }

        public void setPush(boolean push) {
            this.push = push;
        }

        public boolean isEmail() {
            return email;
        }

        public void setEmail(boolean email) {
            this.email = email;
        }
    }
}
