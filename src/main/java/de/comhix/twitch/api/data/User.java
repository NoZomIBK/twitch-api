package de.comhix.twitch.api.data;

import com.google.common.base.MoreObjects;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author Benjamin Beeker
 */
public class User {

    private String type;
    private String name;
    @SerializedName("created_at")
    private Date createdAt;
    @SerializedName("updated_at")
    private Date updatedAt;
    private String logo;
    @SerializedName("_id")
    private int id;
    @SerializedName("display_name")
    private String displayName;
    private String bio;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .add("name", name)
                .add("createdAt", createdAt)
                .add("updatedAt", updatedAt)
                .add("logo", logo)
                .add("id", id)
                .add("displayName", displayName)
                .add("bio", bio)
                .toString();
    }
}
