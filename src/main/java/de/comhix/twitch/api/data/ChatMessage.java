package de.comhix.twitch.api.data;

/**
 * @author Benjamin Beeker
 */
public class ChatMessage {
    private final String sender;
    private final String message;

    public ChatMessage(String sender, String message) {
        this.sender = sender;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getSender() {
        return sender;
    }
}
