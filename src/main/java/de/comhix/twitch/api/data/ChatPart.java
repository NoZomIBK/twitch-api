package de.comhix.twitch.api.data;

/**
 * @author Benjamin Beeker
 */
public class ChatPart {
    private final String user;

    public ChatPart(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
