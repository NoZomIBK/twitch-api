package de.comhix.twitch.api.data;

/**
 * @author Benjamin Beeker
 */
public class ChatJoin {
    private final String user;

    public ChatJoin(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
