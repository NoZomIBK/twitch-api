package de.comhix.twitch.api.irc;

import de.comhix.twitch.api.data.ChatJoin;
import de.comhix.twitch.api.data.ChatMessage;
import de.comhix.twitch.api.data.ChatPart;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import org.apache.commons.lang3.tuple.Pair;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Internal class to connect to an IRC-Chat
 *
 * @author Benjamin Beeker
 */
public class ChatConnector extends PircBot {
    private static final Logger log = LoggerFactory.getLogger(ChatConnector.class);
    private final PublishSubject<String> joinPublisher;
    private final PublishSubject<String> partPublisher;
    private final PublishSubject<Pair<String, String>> messagePublisher;

    public ChatConnector(String name) {
        setName(name);

        joinPublisher = PublishSubject.create();
        partPublisher = PublishSubject.create();
        messagePublisher = PublishSubject.create();
    }

    public Observable<ChatJoin> onJoin() {
        return joinPublisher.publish().autoConnect().map(ChatJoin::new);
    }

    public Observable<ChatPart> onPart() {
        return partPublisher.publish().autoConnect().map(ChatPart::new);
    }

    public Observable<ChatMessage> onMessage() {
        return messagePublisher.publish().autoConnect().map(message -> new ChatMessage(message.getLeft(), message.getRight()));
    }

    @Override
    protected void onConnect() {
        sendRawLine("CAP REQ :twitch.tv/membership");
        super.onConnect();
    }

    @Override
    protected void onUserList(String channel, User[] users) {
        Observable.fromArray(users).subscribe(user -> feedJoin(user.getNick()));
        super.onUserList(channel, users);
    }

    private void feedJoin(String user) {
        joinPublisher.onNext(user);
    }

    @Override
    protected void onJoin(String channel, String sender, String login, String hostname) {
        feedJoin(sender);
        super.onJoin(channel, sender, login, hostname);
    }

    @Override
    protected void onPart(String channel, String sender, String login, String hostname) {
        partPublisher.onNext(sender);
        super.onPart(channel, sender, login, hostname);
    }

    @Override
    protected void onMessage(String channel, String sender, String login, String hostname, String message) {
        messagePublisher.onNext(Pair.of(sender, message));
        super.onMessage(channel, sender, login, hostname, message);
    }

    @Override
    public void log(String s) {
        log.debug(s);
        super.log(s);
    }

    @Override
    protected void onDisconnect() {
        joinPublisher.onComplete();
        partPublisher.onComplete();
        messagePublisher.onComplete();

        super.onDisconnect();
    }
}
