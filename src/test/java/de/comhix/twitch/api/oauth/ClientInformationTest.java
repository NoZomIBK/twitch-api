package de.comhix.twitch.api.oauth;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Benjamin Beeker
 */
public class ClientInformationTest {
    private static final Logger log = LoggerFactory.getLogger(ClientInformationTest.class);

    @Test
    public void testLoadFromJson() throws Exception {
        log.info("testLoadFromJson");

        String json="{\n" +
                "  \"client-id\": \"datId\",\n" +
                "  \"client-secret\": \"datSecret\",\n" +
                "  \"callback-url\": \"http://localhost:8080/oauth/callback\"\n" +
                "}";
        ClientInformation information = new Gson().fromJson(json, ClientInformation.class);

        assertThat(information).isNotNull();
        assertThat(information.getClientId()).isEqualTo("datId");
        assertThat(information.getClientSecret()).isEqualTo("datSecret");
        assertThat(information.getCallbackUrl()).isEqualTo("http://localhost:8080/oauth/callback");
    }
}