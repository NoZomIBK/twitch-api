package de.comhix.async;

import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

/**
 * @author Benjamin Beeker
 */
public class AsyncHelper {
    public static <Type> Type get(Observable<Type> observable) {
        return observable.timeout(30, TimeUnit.SECONDS)
                .blockingFirst();
    }
}
